
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>Login</title>
    <style>
        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: green;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

    </style>
</head>
<body>

<center>
    <div class="container">
        <h1>Login</h1>
        <div class="card">
            <div class="card-body">
                <form action="/controller" method="post">

                    <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
                           value="${AttributeParameterHolder.PARAMETER_COMMAND_LOGIN}"/>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_NAME}" class="col-sm-3 col-form-label">UserName</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="text"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_NAME}"
                                    placeholder="Enter userName:">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                               class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="password"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                                    placeholder="Enter password:"/>
                        </div>
                    </div>
                    <div style="display: flex; justify-content: right">
                        <a class="addBtn"
                           href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_PRE_REGISTER}">
                            Free register
                        </a>

                        <input class="addBtn" type="submit" value="Enter">
                    </div>
                </form>
            </div>
        </div>
    </div>
</center>

</body>
</html>
