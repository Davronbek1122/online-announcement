package uz.pdp.onlineannouncement.service;

import uz.pdp.onlineannouncement.entity.Comment;

import java.util.List;
import java.util.UUID;

public interface CommentService {
    boolean insert(Comment comment);
    boolean delete(UUID id);
    List<Comment> findAll();
    List<Comment> findAllByAnnouncementId(UUID announcementId);
    boolean update(Comment comment);
    Comment findById(UUID id);

}
