package uz.pdp.onlineannouncement.command;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.navigation.Router;

//import javax.servlet.http.HttpServletRequest;

@FunctionalInterface
public interface Command {
    Router execute(HttpServletRequest request);
    default void refresh(HttpServletRequest request){};
}
