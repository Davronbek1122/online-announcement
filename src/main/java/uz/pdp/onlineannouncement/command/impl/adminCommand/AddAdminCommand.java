package uz.pdp.onlineannouncement.command.impl.adminCommand;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.impl.RegisterCommand;
import uz.pdp.onlineannouncement.command.navigation.PageNavigation;
import uz.pdp.onlineannouncement.command.navigation.Router;

public class AddAdminCommand implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        RegisterCommand.addUserOrAdmin(request, true);
        return new Router(PageNavigation.PAGE_ADMIN_HOME_PAGE , Router.PageChangeType.REDIRECT);
    }
}
