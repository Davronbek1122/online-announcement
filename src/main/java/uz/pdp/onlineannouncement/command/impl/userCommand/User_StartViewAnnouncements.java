package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Comment;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.CommentService;
import uz.pdp.onlineannouncement.service.RateService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.CommentServiceImpl;
import uz.pdp.onlineannouncement.service.impl.RateServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.util.List;
import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CABINET;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_VIEW_ANNOUNCEMENT;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_StartViewAnnouncements implements Command
{

    @Override
    public Router execute(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        UUID announcementId = UUID.fromString(request.getParameter(PARAMETER_ANNOUNCEMENT_ID));
        UserService userService = UserServiceImpl.getInstance();
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);

        if (userService.checkToActive(currentUser.getId()))
        {
            AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
            Announcement announcement = announcementService.findById(announcementId);

            User owner = userService.findById(announcement.getUser().getId());

            session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_OWNER, owner);
            session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT, announcement);

            refresh(request);

            return new Router(PAGE_USER_VIEW_ANNOUNCEMENT, REDIRECT);
        }
        session.setAttribute(SESSION_ATTRIBUTE_BLOCKED_MSG, "You are blocked. Please call our admin!");
        return new Router(PAGE_USER_CABINET, REDIRECT);
    }

    @Override
    public void refresh(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Announcement announcement = (Announcement) session.getAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT);
        CommentService commentService = CommentServiceImpl.getInstance();
        List<Comment> allByAnnouncementId = commentService.findAllByAnnouncementId(announcement.getId());

        RateService rateService = RateServiceImpl.getInstance();
        User owner = (User) session.getAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_OWNER);

        session.setAttribute(SESSION_ATTRIBUTE_RATES_TO_USER, rateService.findAllByToUserId(owner.getId()));
        session.setAttribute(SESSION_ATTRIBUTE_COMMENT_LIST, allByAnnouncementId);
    }
}
