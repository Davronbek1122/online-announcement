package uz.pdp.onlineannouncement.entity.enums;

public enum RoleName {
    ADMIN,
    USER,
    GUEST;
}
