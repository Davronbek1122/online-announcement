package uz.pdp.onlineannouncement.entity;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.UUID;

public class Rate extends AbstractEntity{
    private User toUser;
    private User fromUser;
    private double rate;

    public Rate(UUID id, String name, User toUser, User fromUser, double rate, Timestamp timestamp) {
        super(id, name, timestamp);
        this.toUser = toUser;
        this.fromUser = fromUser;
        this.rate = rate;
    }

    public Rate() {

    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

}
