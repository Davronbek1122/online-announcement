package uz.pdp.onlineannouncement.entity;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.UUID;

public class User extends AbstractEntity{
    private String roleName;
    private String username;
    private String password;
    private String phoneNumber;
    private boolean active;
    private double avgRate;

    public User()
    {

    }

    public User(UUID id, String name, String roleName, String username, String password, String phoneNumber, boolean active, double avgRate, Timestamp createdAt) {
        super(id, name, createdAt);
        this.roleName = roleName;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.active = active;
        this.avgRate = avgRate;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(double avgRate) {
        this.avgRate = avgRate;
    }

}
