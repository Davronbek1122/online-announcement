package uz.pdp.onlineannouncement.entity;

import java.sql.Timestamp;
import java.util.UUID;

public abstract class AbstractEntity {
    private UUID id;
    private String name;

    private Timestamp createdAt;

    public AbstractEntity(UUID id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public AbstractEntity(UUID id, String name, Timestamp createdAt) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt;
    }

    public AbstractEntity()
    {
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
