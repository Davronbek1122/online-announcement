package uz.pdp.onlineannouncement.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder;
import uz.pdp.onlineannouncement.command.navigation.PageNavigation;
import uz.pdp.onlineannouncement.entity.User;

import java.io.IOException;

@WebFilter(filterName = "PreControllerFilter", urlPatterns ={ "/controller", "/pages/controller"})
public class PreControllerFilter implements Filter
{
    static Logger logger = LogManager.getLogger();
    public void init(FilterConfig config) throws ServletException
    {

    }

    public void destroy()
    {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException
    {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession(false);
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        logger.log(Level.INFO, "++++++++++> Session in PreControllerFilter : "+ (session != null ? session.getId() : "SessionNotCreated"));
        chain.doFilter(request, response);
    }
}
