package uz.pdp.onlineannouncement.util.mapper;

import uz.pdp.onlineannouncement.entity.Rate;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RateMapper {

    public static final RateMapper instance = new RateMapper();

    public static RateMapper getInstance(){
        return instance;
    }

    private RateMapper() {
    }

    public static List<Rate> convertResultSetToRateList (ResultSet resultSet) throws SQLException {
        List<Rate> rateList = new ArrayList<>();
        UserService userService = UserServiceImpl.getInstance();
        while (resultSet.next()){
            Rate rate = new Rate();
            rate.setId((UUID) resultSet.getObject(1));
            rate.setToUser(userService.findById((UUID) resultSet.getObject(2)));
            rate.setFromUser(userService.findById((UUID) resultSet.getObject(3)));
            rate.setRate(resultSet.getDouble(4));
            rate.setCreatedAt(resultSet.getTimestamp(5));
            rateList.add(rate);
        }
        return rateList;
    }
}
