package uz.pdp.onlineannouncement.util.mapper;

import uz.pdp.onlineannouncement.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserMapper {
    private static final UserMapper userMapper= new UserMapper();

    private UserMapper() {
    }

    public static UserMapper getInstance(){
        return userMapper;
    }

    public static List<User> convertResultSetToUserList(ResultSet resultSet) throws SQLException {
        List<User> users = new ArrayList<>();
        while (resultSet.next()){
            User user = new User();
            user.setId((UUID) resultSet.getObject(1));
            user.setRoleName(resultSet.getString(2));
            user.setName(resultSet.getString(3));
            user.setUsername(resultSet.getString(4));
            user.setPassword(resultSet.getString(5));
            user.setActive(resultSet.getBoolean(6));
            user.setPhoneNumber(resultSet.getString(7));
            user.setAvgRate(Math.round(resultSet.getDouble(8)));
            user.setCreatedAt(resultSet.getTimestamp(9));
            users.add(user);
        }
        return users;
    }
}
