package uz.pdp.onlineannouncement.dao.impl;

import uz.pdp.onlineannouncement.dao.UserDao;
import uz.pdp.onlineannouncement.util.mapper.UserMapper;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class UserDaoImpl implements UserDao {

    private static final UserDaoImpl instance = new UserDaoImpl();

    public static UserDaoImpl getInstance()
    {
        return instance;
    }

    private UserDaoImpl() {
    }

    private ConnectionPool connectionPool = ConnectionPool.getIntance();
    @Override
    public boolean insert(User user)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "insert into users(id, role_name, fio, user_name, password, active, phone_number, avg_rate, created_at) VALUES (?,?,?,?,?,?,?,?,?) ;");
            preparedStatement.setObject(1,user.getId());            // id
            preparedStatement.setObject(2,user.getRoleName());      // role_name
            preparedStatement.setString(3, user.getName());         // fio
            preparedStatement.setString(4, user.getUsername());     // user_name
            preparedStatement.setString(5, user.getPassword());     // password
            preparedStatement.setBoolean(6, user.isActive());       // active
            preparedStatement.setString(7, user.getPhoneNumber());  // phone_number
            preparedStatement.setDouble(8,user.getAvgRate());       // avg_rate
            preparedStatement.setTimestamp(9,user.getCreatedAt());       // created_at
            preparedStatement.execute();
            return true;

        } catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean delete(User user)
    {
        return false;
    }

    @Override
    public List<User> findAll()
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from users order by fio order by created_at");
            return UserMapper.convertResultSetToUserList(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<User> findAllWithoutUser(UUID userId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from users where id <> ? order by created_at;");
            preparedStatement.setObject(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return UserMapper.convertResultSetToUserList(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(User user)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "update users set fio = ?, user_name = ?, password = ?, phone_number = ? where id = ?;");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getUsername());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getPhoneNumber());
            preparedStatement.setObject(5, user.getId());
            preparedStatement.execute();
            return true;
        } catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean authenticate(String login, String password)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select password from users where user_name = ?;");
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            String passwordDb = null;
            while (resultSet.next())
            {
                passwordDb = resultSet.getString(1);
            }
            return password.equals(passwordDb);
        } catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean checkToAdmin(String user_name, String password)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from users where role_name = 'ADMIN' and user_name = ? and password = ?;");
            preparedStatement.setString(1,user_name);
            preparedStatement.setString(2,password);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean checkToActive(UUID userId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from users where id = ? and active = true;");
            preparedStatement.setObject(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public User findById(UUID id)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from users where id = ? ;");
            preparedStatement.setObject(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<User> users = UserMapper.convertResultSetToUserList(resultSet);
            return (!users.isEmpty()) ? users.get(0) : null;
        } catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public User findByUserName(String userName)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from users where user_name = ? ;");
            preparedStatement.setString(1,userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<User> users = UserMapper.convertResultSetToUserList(resultSet);
            return !users.isEmpty() ? users.get(0) : null;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean blockOrActive(UUID userId, boolean setActive)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("update users set active = ? where id = ? ;");
            preparedStatement.setBoolean(1,setActive);
            preparedStatement.setObject(2,userId);
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }
}
